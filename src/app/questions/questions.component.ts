import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
	selector: 'app-questions',
	templateUrl: './questions.component.html',
	styleUrls: ['./questions.component.styl']
})
export class QuestionsComponent implements OnInit {
	@Output() successfullyPassed = new EventEmitter<void>();
	@ViewChild('questionsElement') private questionsElement: ElementRef<HTMLElement>;
	public questions: Question[];
	private readyToSubmit$ = new Subject<void>();

	constructor() {
		this.questions = [
			{label: 'Uveďte své celé jméno', answer: 'Eduard Pelikán', correct: false},
			{label: 'Jak se jmenuje značka auta, kterou vlastníte?', answer: 'Porsche Taycan', correct: false},
			{label: 'Uveďte váš oblíbený film', answer: 'Star Trek', correct: false}
		];
	}

	ngOnInit(): void {

		this.readyToSubmit$
			.pipe(first())
			.subscribe(e => {
				this.questionsElement.nativeElement.classList.add('animate__animated', 'animate__fadeOutUp');

				this.questionsElement.nativeElement.addEventListener('animationend', e => {
					this.successfullyPassed.emit();
				});
			});
	}

	compareQAndA(value: string, question: Question) {
		question.correct = value === question.answer;
	}

	submit() {
		let correct = true;
		this.questions.forEach((question, i) => {
			correct = correct && question.correct;
		});

		if (correct) {
			this.readyToSubmit$.next();
		}

	}
}

interface Question {
	label: string;
	answer: string;
	correct: boolean;
}
