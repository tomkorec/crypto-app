import {ChangeDetectorRef, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Component({
	selector: 'app-crazy',
	templateUrl: './crazy.component.html',
	styleUrls: ['./crazy.component.styl']
})
export class CrazyComponent implements OnInit {
	@Output() successfullyPassed = new EventEmitter<void>();
	@ViewChild('crazyBox') private crazyBox: ElementRef<HTMLElement>;
	public buttons = [
		'Dokumenty',
		'@Eduard',
		'Tajné dokumenty',
		'Tajná složka',
		'Ještě tajněší složka',
		'Supertajná složka',
		'Nejtajnější složka',
		'Fakt hodně moc tajná složka',
		'Nechodit!!!!!',
		'Kočičky',
		'Jdi pryč!',
		'A už se nevracej!',
		'Tady to je',
		'Ne, tady',
		'Tady už to fakt je',
		'TOP SECRET',
	];
	private buttonsCount: number;
	public currentIndex = 0;
	private finalize$ = new Subject<void>();

	constructor(private changeDetectorRef: ChangeDetectorRef) {
	}

	ngOnInit(): void {
		this.buttonsCount = this.buttons.length;

		this.finalize$
			.subscribe(() => {
				this.crazyBox.nativeElement.classList.add('animate__animated', 'animate__bounceOut');
				this.crazyBox.nativeElement.addEventListener('animationend', e => {
					this.successfullyPassed.emit();
				});
			});
	}

	openFolder(index: number) {
		console.log('doubel', this.currentIndex);
		this.currentIndex = index + 1;
		console.log('doubel', this.currentIndex);
		this.changeDetectorRef.detectChanges();
		if (this.currentIndex >= this.buttonsCount) {
			this.finalize$.next();
		}
	}
}

