import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {filter} from 'rxjs/operators';

@Component({
	selector: 'app-lockscreen',
	templateUrl: './lockscreen.component.html',
	styleUrls: ['./lockscreen.component.styl']
})
export class LockscreenComponent implements OnInit {
	changeCode$ = new BehaviorSubject<string>('');
	private readonly CODE = '12ML5slk13m443';
	@Output() successfullyPassed = new EventEmitter<void>();
	@ViewChild('lockscreen') private lockscreen: ElementRef<HTMLElement>;


	constructor() {
	}

	ngOnInit(): void {

		this.changeCode$
			.pipe(
				filter(code => code === this.CODE)
			)
			.subscribe(_ => {
				this.lockscreen.nativeElement.classList.add('animate__animated', 'animate__bounceOut');

				this.lockscreen.nativeElement.addEventListener('animationend', e => {
					this.successfullyPassed.emit();
				});

			});
	}

}
