import {AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {filter, tap} from 'rxjs/operators';

@Component({
	selector: 'app-crypto',
	templateUrl: './crypto.component.html',
	styleUrls: ['./crypto.component.styl']
})
export class CryptoComponent implements OnInit, AfterViewInit {
	changeCode$ = new BehaviorSubject<string>('');
	private readonly CODE = 'Edán';
	@Output() successfullyPassed = new EventEmitter<void>();
	@ViewChild('cryptoElement') private cryptoElement: ElementRef<HTMLElement>;
	@ViewChild('morseEl') private morse: ElementRef<HTMLElement>;

	constructor() {
	}

	ngOnInit(): void {


		this.changeCode$
			.pipe(
				filter(code => code === this.CODE)
			)
			.subscribe(_ => {
				this.cryptoElement.nativeElement.classList.add('animate__animated', 'animate__bounceOut');

				this.cryptoElement.nativeElement.addEventListener('animationend', e => {
					this.successfullyPassed.emit();
				});

			});

	}

	ngAfterViewInit() {
		this.morse.nativeElement.addEventListener('contextmenu', function(evt) {
			evt.preventDefault();
		}, false);

		this.morse.nativeElement.addEventListener('copy', function(evt) {
			evt.clipboardData.setData('text/plain', 'Mě nezkopíruješ, plantážníku! 😁');
			evt.preventDefault();
		}, false);
	}


}
