import {Component} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.styl']
})
export class AppComponent {
	title = 'crypto';
	public stage: Stage = 'lockscreen';

	constructor(private cookieService: CookieService) {
		let stageCookie = this.cookieService.get('stage');
		if (stageCookie) {
			this.stage = <Stage> this.cookieService.get('stage');
		}
	}

	public lockscreenPassed() {
		this.stage = 'questions';
		this.cookieService.set('stage', 'questions', 1);
	}

	public questionsPassed() {
		this.stage = 'crypto';
		this.cookieService.set('stage', 'crypto', 1);
	}

	public cryptoPassed() {
		this.stage = 'crazy';
		this.cookieService.set('stage', 'crazy', 1);
	}

	public crazyPassed() {
		this.stage = 'address';
		this.cookieService.set('stage', 'address', 1);
	}
}

type Stage = 'lockscreen' | 'questions' | 'crypto' | 'crazy' | 'address';
