import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LockscreenComponent} from './lockscreen/lockscreen.component';
import {FormsModule} from '@angular/forms';
import {QuestionsComponent} from './questions/questions.component';
import {CookieService} from 'ngx-cookie-service';
import {AddressComponent} from './address/address.component';
import {CryptoComponent} from './crypto/crypto.component';
import {CrazyComponent} from './crazy/crazy.component';

@NgModule({
	declarations: [
		AppComponent,
		LockscreenComponent,
		QuestionsComponent,
		AddressComponent,
		CryptoComponent,
		CrazyComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule
	],
	providers: [CookieService],
	bootstrap: [AppComponent]
})
export class AppModule {
}
